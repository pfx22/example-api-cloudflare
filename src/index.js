/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `npm run dev` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `npm run deploy` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

export default {
	async fetch(request) {
	  const data = {
		Status: "🟢 Online",
		Title: "Exemplo simples de API usando Workers-CloudFlare",
	  };
  
	  const json = JSON.stringify(data, null, 2);
	  
	  return new Response(json, {
		headers: {
		  "content-type": "application/json;charset=UTF-8",
		},
	  });
	},
  };